﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class DateTimeHelper
    {

        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        {
            DateTime startOfThisPeriod = start;
            while (startOfThisPeriod < end)
            {
                DateTime endOfThisPeriod = startOfThisPeriod.AddDays(dayChunkSize);
                endOfThisPeriod = endOfThisPeriod < end ? endOfThisPeriod : end;
                yield return Tuple.Create(startOfThisPeriod, endOfThisPeriod);
                startOfThisPeriod = endOfThisPeriod;
            }
        }

        public static IEnumerable<DateTimeRange> GetConsultas(DateTime inicial, DateTime final)
        {

            if (inicial > final)
                throw new Exception("Inicial > Final");

            if (final < inicial)
                throw new Exception("Final < Inicial");

            if (inicial.Date.Equals(final.Date))
            {

                yield return new DateTimeRange { StartDateTime = inicial, EndDateTime = final };

            }
            else
            {

                var cInicial = inicial;

                while (cInicial < final)
                {

                    var cFinal = cInicial.AddTicks(863990000000);

                    cFinal = cFinal < final ? cFinal : final;

                    var firstTimeOfDay = cInicial.Date.Equals(inicial.Date) ?
                        inicial.TimeOfDay : TimeSpan.FromTicks(0);

                    var lastTimeOfDay = cInicial.Date.Equals(final.Date) ?
                        final.TimeOfDay : TimeSpan.FromTicks(863990000000);

                    var parametrosConsulta = new DateTimeRange
                    {
                        StartDateTime = cInicial.Date.Add(firstTimeOfDay),
                        EndDateTime = cInicial.Date.Add(lastTimeOfDay)
                    };

                    yield return parametrosConsulta;

                    cInicial = cFinal.AddTicks(10000000);

                }

            }

        }

        public static List<Tuple<DateTime, DateTime>> PartirRangoDeFechasEnDias(DateTime fechaInicial, DateTime fechaFinal)
        {
            List<Tuple<DateTime, DateTime>> listaTiempos = new List<Tuple<DateTime, DateTime>>();

            long Fi = ((DateTimeOffset)fechaInicial).ToUnixTimeSeconds();
            long Ff = ((DateTimeOffset)fechaFinal).ToUnixTimeSeconds();

            Debug.WriteLine("Fi: " + Fi.ToString());
            Debug.WriteLine("Ff: " + Ff.ToString());

            TimeSpan timeSpanEntreFechas = fechaFinal - fechaInicial;  //1009799000
            Debug.WriteLine("timeSpanEntreFechas: " + timeSpanEntreFechas.TotalSeconds);

            int diasTimeSpan = (int)Math.Floor((fechaFinal - fechaInicial).TotalDays);
            Debug.WriteLine("diasTimeSpan: " + diasTimeSpan.ToString());

            long FiAux = Fi;
            long FfAux = Ff;

            for (int i = 0; i <= diasTimeSpan; i++)
            {
                if (i < diasTimeSpan)
                {
                    DateTime FiDT = UnixTimeStampToDateTime(FiAux);
                    DateTime Dh = new DateTime(FiDT.Year, FiDT.Month, FiDT.Day, 0, 0, 0);
                    long Fh = ((DateTimeOffset)Dh).ToUnixTimeSeconds();

                    long fx = 86399 + Fh;
                    DateTime fechaFinalEncontrada = UnixTimeStampToDateTime(fx);
                    Debug.WriteLine(FiDT.ToString("dd/MM/yyyy HH:mm:ss"));
                    Debug.WriteLine(fechaFinalEncontrada.ToString("dd/MM/yyyy HH:mm:ss"));
                    listaTiempos.Add(new Tuple<DateTime, DateTime>(FiDT, fechaFinalEncontrada));
                    FiAux = fx + 1;
                }
                else
                {
                    DateTime FiDT = UnixTimeStampToDateTime(FiAux);
                    long fx = FfAux;

                    DateTime fechaFinalEncontrada = UnixTimeStampToDateTime(fx);
                    Debug.WriteLine(FiDT.ToString("dd/MM/yyyy HH:mm:ss"));
                    Debug.WriteLine(fechaFinalEncontrada.ToString("dd/MM/yyyy HH:mm:ss"));
                    listaTiempos.Add(new Tuple<DateTime, DateTime>(FiDT, fechaFinalEncontrada));
                }
            }

            return listaTiempos;
        }

        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))  // Days: 1, 2 ... 31 etc.
                             .Select(day => new DateTime(year, month, day)) // Map each day to a date
                             .ToList(); // Load dates into a list
        }

        static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
