﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class RFC
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [StringLength(13)]
        [Index("IDX_UNIQUE_RFC", IsUnique = true)]
        public string rfc { get; set; }
        public int clienteId{ get; set; }
        public virtual Cliente cliente { get; set; }
        public string razon_social { get; set; }
        public int estadoRFC { get; set; }
        public byte[] certificado { get; set; }
        public byte[] clave_privada { get; set; }
        public string password_key { get; set; }
        public DateTime fecha_registro { get; set; }
        public DateTime fecha_actualizacion { get; set; }
    }
    public class xVar
    {
        public static string imgB64;
    }
}
