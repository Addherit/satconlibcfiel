﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class FacturasRecibidasParser
    {
        public static List<FacturaRecibidaMetadata> GetListaFacturasRecibidas(string htmlFacturasEmitidasResult)
        {
            List<FacturaRecibidaMetadata> listaFacRecibidas = new List<FacturaRecibidaMetadata>();
            string fixedFacturasRecibidasResultHtml = Regex.Replace(htmlFacturasEmitidasResult, @"title=\""Seleccionar todos\"" \/>&nbsp;&nbsp;Acciones<\/span>\s*<\/div>", @"title=\""Seleccionar todos\"" \/>&nbsp;&nbsp;Acciones<\/span>");

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(fixedFacturasRecibidasResultHtml);

            HtmlNode TableResults = doc.GetElementbyId("ctl00_MainContent_tblResult");
            var trElements = TableResults.SelectNodes("tr").Where(x => x.ChildNodes.Any(y => y.Name == "th") == false);

            Regex regexDescargaXML = new Regex(@"return AccionCfdi\('RecuperaCfdi.aspx\?Datos=((.)+?)'");
            Regex regexDescargaPDF = new Regex(@"recuperaRepresentacionImpresa\('((.)+?)'");

            foreach (HtmlNode row in trElements)
            {
                HtmlNodeCollection cells = row.SelectNodes("td");

                if (cells == null)
                {
                    continue;
                }

                FacturaRecibidaMetadata factMetadata = new FacturaRecibidaMetadata();

                var htmlLinksCell = cells[0].OuterHtml.Trim();
                var descargaXMLMatch = regexDescargaXML.Match(htmlLinksCell);
                var descargaPDFMatch = regexDescargaPDF.Match(htmlLinksCell);
                string cadenaDescargaXML = string.Empty;
                string cadenaDescargaPDF = string.Empty;

                if (descargaXMLMatch != null)
                    cadenaDescargaXML = descargaXMLMatch.Groups[1].Value;

                if (descargaPDFMatch != null)
                    cadenaDescargaPDF = descargaPDFMatch.Groups[1].Value;


                factMetadata.XmlDataDescarga = cadenaDescargaXML;
                factMetadata.PdfDataDescarga = cadenaDescargaPDF;
                factMetadata.FolioFiscal = new Guid(cells[1].InnerText.Trim());
                factMetadata.RfcEmisor = cells[2].InnerText.Trim();
                factMetadata.RazonSocialEmisor = cells[3].InnerText.Trim();
                factMetadata.RfcReceptor = cells[4].InnerText.Trim();
                factMetadata.RazonSocialReceptor = cells[5].InnerText.Trim();
                factMetadata.FechaDeEmision = DateTime.ParseExact(cells[6].InnerText.Trim(), "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                factMetadata.FechaDeCertificacion = DateTime.ParseExact(cells[7].InnerText.Trim(), "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                factMetadata.PacQueCertifico = cells[8].InnerText.Trim();

                NumberFormatInfo FormatInfo = new NumberFormatInfo();
                FormatInfo.CurrencyGroupSeparator = ",";
                FormatInfo.CurrencySymbol = "$";
                decimal parsedTotal = decimal.Parse(cells[9].InnerText.Trim(), NumberStyles.Currency, FormatInfo);

                factMetadata.Total = parsedTotal;
                factMetadata.EfectoDelComprobante = cells[10].InnerText.Trim();
                factMetadata.EstatusDeCancelacion = cells[11].InnerText.Trim();
                factMetadata.EstadoDelComprobante = cells[12].InnerText.Trim();
                factMetadata.EstatusDeProcesoDeCancelacion = cells[13].InnerText.Trim();
                factMetadata.FechaCreacion = DateTime.Now;
                factMetadata.FechaActualizacion = DateTime.Now;

                DateTime? fechaDeProcesoDeCancelacion = null;
                if (!string.IsNullOrEmpty(cells[14].InnerText.Trim()))
                    fechaDeProcesoDeCancelacion = DateTime.ParseExact(cells[14].InnerText.Trim(), "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                factMetadata.FechaDeProcesoDeCancelacion = fechaDeProcesoDeCancelacion;

                listaFacRecibidas.Add(factMetadata);
            }
            return listaFacRecibidas;
        }
    }
}
