﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class Logs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [StringLength(50)]
        public string MachineName { get; set; }
        public DateTime Logged { get; set; }
        [StringLength(50)]
        public string Level { get; set; }
        [Column(TypeName = "text")]
        public string Message { get; set; }
        [StringLength(250)]
        public string Logger { get; set; }
        [Column(TypeName = "text")]
        public string Callsite { get; set; }
        [Column(TypeName = "text")]
        public string Exception { get; set; }

    }
}
