﻿using Flurl.Http;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SATCONLIB
{
    public class FacturasRecibidasManager
    {
        private readonly IFlurlClient _client;
        private readonly ISatConnection _connection;
        private bool _loggedIn;
        private SynchronizedCollection<FacturaRecibidaMetadata> _cfdisDescargados;

        public FacturasRecibidasManager(ref IFlurlClient client, ref ISatConnection connection)
        {
            this._client = client;
            this._connection = connection;
            this._loggedIn = false;
            this._cfdisDescargados = new SynchronizedCollection<FacturaRecibidaMetadata>();
        }

        public bool IsLoggedIn()
        {
            return this._loggedIn;
        }

        public async System.Threading.Tasks.Task<bool> LoginAsync()
        {
            try
            {
                await this._connection.CerrarSesion();
            }
            catch (Exception ex) { }

            bool loginResult = await this._connection.LoginAsync();
            this._loggedIn = loginResult;
            return this._loggedIn;
        }

        private async System.Threading.Tasks.Task<string> GetFacturasRecibidasPortalHtmlAsync()
        {
            var responseConsultaReceptor = await URLS.URL_CONSULTA_RECEPTOR.WithClient(this._client).WithHeaders(Headers.HeadersConsulta).GetAsync();
            var responseConsultaReceptorContent = await responseConsultaReceptor.GetStringAsync();
            return responseConsultaReceptorContent;
        }

        private List<KeyValuePair<string, string>> GetFormInputElements(string html)
        {
            var formParameters = new List<KeyValuePair<string, string>>();

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(html);

            var formElements = doc.GetElementsByTagName("form");

            HtmlNode form = null;

            if (formElements != null)
                form = formElements.FirstOrDefault();

            if (form != null)
            {

                foreach (HtmlNode node in form.GetElementsByTagName("input"))
                {
                    HtmlAttribute nameAttribute = node.Attributes["name"];
                    HtmlAttribute valueAttribute = node.Attributes["value"];

                    if (nameAttribute != null && valueAttribute != null)
                    {
                        formParameters.Add(new KeyValuePair<string, string>(nameAttribute.Value, valueAttribute.Value));
                    }
                }
            }

            return formParameters;
        }

        private async System.Threading.Tasks.Task<string> GetFacturasRecibidasSolicitudPrevia(string htmlFacturasRecibidasPortal, DateTime fechaInicial)
        {
            var formInputElements = GetFormInputElements(htmlFacturasRecibidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$RdoFechas"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", "ctl00$MainContent$RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            var viewState = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATE"); ;
            if (!viewState.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(viewState.Key, viewState.Value));
            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;
            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlAnio", fechaInicial.Year.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMes", fechaInicial.Month.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlDia", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlHoraFin", "23"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMinutoFin", "59"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlSegundoFin", "59"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));
            var responseConsultaReceptor = await URLS.URL_CONSULTA_RECEPTOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedRecibidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();
            return responseConsultaReceptor;
        }

        /*        public async System.Threading.Tasks.Task ObtenerListaDeFechasDeDescarga(DateTime PrimerFecha, DateTime SegundaFecha)
                {

                    Ref<List<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<List<Tuple<DateTime, DateTime>>>
                    {
                        Value = new List<Tuple<DateTime, DateTime>>()
                    };

                    await ObtenerCantidadDeFacturas(IntervalTimeList, PrimerFecha, SegundaFecha);
                }*/

        public async Task ObtenerCantidadDeFacturas(Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> intervalTimeList, DateTime fechaInicial, DateTime fechaFinal)
        {
           // Console.WriteLine(String.Format("\nHaciendo peticion de {0} a {1}...", fechaInicial.ToString("dd/MM/yyyy HH:mm:ss"), fechaFinal.ToString("dd/MM/yyyy HH:mm:ss")));
            string responseGetFacturasRecibidas = await GetFacturasRecibidasEnRangoDeFecha(fechaInicial, fechaFinal);
            bool respuestaExitosa = responseGetFacturasRecibidas.Contains("|updatePanel|ctl00_MainContent_UpnlBusqueda|");

            if (!respuestaExitosa)
            {
                await ObtenerCantidadDeFacturas(intervalTimeList, fechaInicial, fechaFinal);
            }

            bool masDeQuinientos = responseGetFacturasRecibidas.Contains("ctl00_MainContent_PnlLimiteRegistros");

            if (!masDeQuinientos)
            {
                intervalTimeList.Value.Add(new Tuple<DateTime, DateTime>(fechaInicial, fechaFinal));
                return;
            }

            long HalfTimeSpan = (fechaFinal.Ticks - fechaInicial.Ticks) / 2;
            TimeSpan FirstTimeSpanHalf = new TimeSpan(fechaInicial.Ticks + HalfTimeSpan);
            DateTime FirstTimeHalf = DateTime.MinValue + FirstTimeSpanHalf;
            TimeSpan SecondTimeSpanHalf = new TimeSpan(fechaFinal.Ticks - HalfTimeSpan);
            DateTime SecondTimeHalf = (DateTime.MinValue + SecondTimeSpanHalf).AddSeconds(1);
            await ObtenerCantidadDeFacturas(intervalTimeList, fechaInicial, FirstTimeHalf);
            await ObtenerCantidadDeFacturas(intervalTimeList, SecondTimeHalf, fechaFinal);
        }

        public async System.Threading.Tasks.Task<string> GetFacturasRecibidasEnRangoDeFecha(DateTime fechaInicial, DateTime fechaFinal)
        {
            var htmlFacturasRecibidasPortal = await GetFacturasRecibidasPortalHtmlAsync();
            //await ObtenerUsuarioRecibidas();
            var htmlConsultaPrevia = await GetFacturasRecibidasSolicitudPrevia(htmlFacturasRecibidasPortal, fechaInicial);
            //await ObtenerUsuarioRecibidas();
            var formInputElements = GetFormInputElements(htmlFacturasRecibidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();
            await this._connection.MantieneSesionEmitidas(DateTime.Now);

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$BtnBusqueda"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlAnio", fechaInicial.Year.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMes", fechaInicial.Month.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlDia", fechaInicial.Day.ToString().PadLeft(2, '0')));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlHora", fechaInicial.Hour.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMinuto", fechaInicial.Minute.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlSegundo", fechaInicial.Second.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlHoraFin", fechaFinal.Hour.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlMinutoFin", fechaFinal.Minute.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFecha$DdlSegundoFin", fechaFinal.Second.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "false"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));

            string viewStatePattern = @"__VIEWSTATE\|(.*?)\|";
            Regex viewStateRegex = new Regex(viewStatePattern);
            Match viewStateMatch = viewStateRegex.Match(htmlConsultaPrevia);
            if (viewStateMatch.Success)
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATE", viewStateMatch.Groups[1].Value));

            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;

            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));

            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$BtnBusqueda", "Buscar CFDI"));
            
            //Recibe html con os xml
            var responseConsultaEmisor = await URLS.URL_CONSULTA_RECEPTOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedRecibidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();

            return responseConsultaEmisor;
        }

        public async System.Threading.Tasks.Task<string> GetFacturasRecibidasPorFolioFiscal(string htmlConsultaPrevia, string folioFiscal)
        {
            var htmlFacturasRecibidasPortal = await GetFacturasRecibidasPortalHtmlAsync();
            var formInputElements = GetFormInputElements(htmlFacturasRecibidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$BtnBusqueda"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            string viewStatePattern = @"__VIEWSTATE\|(.*?)\|";
            Regex viewStateRegex = new Regex(viewStatePattern);
            Match viewStateMatch = viewStateRegex.Match(htmlConsultaPrevia);
            if (viewStateMatch.Success)
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATE", viewStateMatch.Groups[1].Value));

            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;

            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));

            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFolioFiscal"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", folioFiscal.ToUpper()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicial", "")); // EL anyo de la fecha inicial?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFinal", "")); // El anyo de la fecha final?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDatos", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFlag", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfAux", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfCancelacion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfUrlDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hdnValAccion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfXML", ""));




            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$BtnBusqueda", "Buscar CFDI"));

            var responseConsultaEmisor = await URLS.URL_CONSULTA_RECEPTOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedRecibidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();

            return responseConsultaEmisor;
        }

        public async System.Threading.Tasks.Task<string> CerrarSesionAsync()
        {
            return await this._connection.CerrarSesion();
        }

        public async System.Threading.Tasks.Task<string> DescargarArchivoXML(FacturaRecibidaMetadata facturaRecibida, string fileSavePath)
        {
           
            string downloadPath = string.Empty;
            if (!string.IsNullOrEmpty(facturaRecibida.XmlDataDescarga))
            {
                downloadPath = await this._connection.DescargarArchivoXML(facturaRecibida.XmlDataDescarga, fileSavePath);
            }

            return downloadPath;
        }

        public async System.Threading.Tasks.Task<string> ObtenerUsuarioRecibidas()
        {
            var responseReceptorObtieneUsuario = await URLS.URL_CONSULTA_RECEPTOR_OBTIENE_USUARIO.WithClient(this._client).WithHeaders(Headers.HeadersObtieneUsuarioReceptor).PostAsync(null);
            var responseReceptorObtieneUsuarioContent = await responseReceptorObtieneUsuario.GetStringAsync();
            return responseReceptorObtieneUsuarioContent;
        }

        public List<string> ObtenerCfdisDescargadosEnRangoDeFechas(DateTime fechaInicial, DateTime fechaFinal, string receptorRFC)
        {
            using (FacturasDescargadasEntities ctx = new FacturasDescargadasEntities(ConnectionData.ConnectionString, ConnectionData.Schema))
            {
                ctx.Database.CommandTimeout = 36000;
                List<string> ListaFoliosFiscalesDescargados = new List<string>();
                var ListaDeFacturasRecibidas = ctx.Comprobantes.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal && x.Receptor.Rfc.ToUpper() == receptorRFC.ToUpper());
                foreach (var cfdi in ListaDeFacturasRecibidas)
                {
                    ListaFoliosFiscalesDescargados.Add(cfdi.Complemento.TimbreFiscalDigital.UUID.ToString().ToUpper());
                }
                return ListaFoliosFiscalesDescargados;
            }
        }

        public SynchronizedCollection<FacturaRecibidaMetadata> ObtenerCFDISDescargadosEnSesion()
        {
            return this._cfdisDescargados;
        }

        public async System.Threading.Tasks.Task ObtenerNumeroFacturasEnFecha(DateTime PrimerFecha, DateTime SegundaFecha)
        {

            Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>>
            {
                Value = new SynchronizedCollection<Tuple<DateTime, DateTime>>()
            };

            await ObtenerCantidadDeFacturas(IntervalTimeList, PrimerFecha, SegundaFecha);
        }

        public async Task ObtenerNumeroFacturasDia(Ref<Tuple<DateTime, DateTime>> dia, Ref<SynchronizedCollection<FacturaRecibidaMetadata>> facturaCollection)
        {
            DateTime diaFechaInicial = dia.Value.Item1;
            DateTime diaFechaFinal = dia.Value.Item2;

            Console.WriteLine("\nObteniendo numero de facturas de fecha {0} a fecha {1}", diaFechaInicial.ToString("dd/MM/yyyy HH:mm:ss"), diaFechaFinal.ToString("dd/MM/yyyy HH:mm:ss"));

            Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>>
            {
                Value = new SynchronizedCollection<Tuple<DateTime, DateTime>>()
            };

            await ObtenerCantidadDeFacturas(IntervalTimeList, diaFechaInicial, diaFechaFinal);

            foreach (var periodo in IntervalTimeList.Value)
            {
                var facturasRecibidasEnRango = await GetFacturasRecibidasEnRangoDeFecha(periodo.Item1, periodo.Item2);
                var facturasRecibidasLista = FacturasRecibidasParser.GetListaFacturasRecibidas(facturasRecibidasEnRango);

                foreach (var factura in facturasRecibidasLista)
                {
                    facturaCollection.Value.Add(factura);
                }
            }
        }
                
    }
}
