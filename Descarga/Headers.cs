﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class Headers
    {
        public static readonly Dictionary<string, string> HeadersPortal1 = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" }
        };

        public static readonly Dictionary<string, string> HeadersPortal3 = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" },
        };

        public static readonly Dictionary<string, string> HeadersPortal4 = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Cache-Control", "max-age=0" },
            { "Origin", "https://cfdiau.sat.gob.mx" },
            { "Upgrade-Insecure-Requests", "1" },
            { "Content-Type", "application/x-www-form-urlencoded" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://cfdiau.sat.gob.mx/nidp/app/login?id=SATx509Custom&sid=0&option=credential&sid=0" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" },
        };

        public static readonly Dictionary<string, string> HeadersPortal5 = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Cache-Control", "max-age=0" },
            { "Origin", "https://cfdiau.sat.gob.mx" },
            { "Upgrade-Insecure-Requests", "1" },
            { "Content-Type", "application/x-www-form-urlencoded" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://cfdiau.sat.gob.mx/nidp/app/login?id=SATx509Custom&sid=0&option=credential&sid=0" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" }
        };


        public static readonly Dictionary<string, string> HeadersConsulta = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" }
        };

        public static readonly Dictionary<string, string> HeadersConsultaXRequestedEmitidas = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Origin", "https://portalcfdi.facturaelectronica.sat.gob.mx"},
            { "X-Requested-With", "XMLHttpRequest" },
            { "X-MicrosoftAjax", "Delta=true"},
            { "Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"},
            { "Cache-Control", "no-cache"},
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "*/*" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx"},
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersConsultaXRequestedRecibidas = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Origin", "https://portalcfdi.facturaelectronica.sat.gob.mx"},
            { "X-Requested-With", "XMLHttpRequest" },
            { "X-MicrosoftAjax", "Delta=true"},
            { "Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"},
            { "Cache-Control", "no-cache"},
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "*/*" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx"},
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersConsultaXRequestedMantieneSesion = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Origin", "https://portalcfdi.facturaelectronica.sat.gob.mx"},
            { "X-Requested-With", "XMLHttpRequest" },
            { "Cache-Control", "no-cache"},
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "*/*" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx"},
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersEmitidasCerrarSesion = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersObtieneUsuarioEmisor = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "Origin", "https://portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Content-Length", "0" },
            { "X-Requested-With", "XMLHttpRequest" },
            { "Content-Type", "application/json; charset=UTF-8" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "*/*" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersObtieneUsuarioReceptor = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "Origin", "https://portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Content-Length", "0" },
            { "X-Requested-With", "XMLHttpRequest" },
            { "Content-Type", "application/json; charset=UTF-8" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "*/*" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersCerrarSesion = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static readonly Dictionary<string, string> HeadersDescargaEmitidos = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Referer", "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8,en;q=0.7" }
        };

        public static class HeadersCiec
        {
            public static readonly Dictionary<string, string> HeadersPortalInicio = new Dictionary<string, string>() {
            { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
            { "Sec-Fetch-Site", "none" },
            { "Sec-Fetch-Mode", "navigate" },
            { "Sec-Fetch-User", "?1" },
            { "Sec-Fetch-Dest", "document" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es,en;q=0.9" }
        };

            public static readonly Dictionary<string, string> HeadersPortal1 = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Cache-Control", "max-age=0" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" }
        };

            public static Dictionary<string, string> GetHeadersPortal2(string refererUrl)
            {
                Dictionary<string, string> HeadersPortal2 = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Content-Length", "0" },
            { "Cache-Control", "max-age=0" },
            { "Upgrade-Insecure-Requests", "1" },
            { "Origin", "https://cfdiau.sat.gob.mx" },
            { "Content-Type", "application/x-www-form-urlencoded" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
            { "Sec-Fetch-Site", "same-origin" },
            { "Sec-Fetch-Mode", "navigate" },
            { "Sec-Fetch-Dest", "document" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es-US,es-419;q=0.9,es;q=0.8" },
            { "Referer", refererUrl }
            };
                return HeadersPortal2;
            }

            public static Dictionary<string, string> GetHeadersLogin(int contentLength)
            {
                Dictionary<string, string> HeadersLogin = new Dictionary<string, string>() {
                    { "Host", "cfdiau.sat.gob.mx" },
                    { "Connection", "keep-alive" },
                    { "Content-Length", contentLength.ToString() },
                    { "Cache-Control", "max-age=0" },
                    { "Upgrade-Insecure-Requests", "1" },
                    { "Origin", "https://cfdiau.sat.gob.mx" },
                    { "Content-Type", "application/x-www-form-urlencoded" },
                    { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
                    { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
                    { "Sec-Fetch-Site", "same-origin" },
                    { "Sec-Fetch-Mode", "navigate" },
                    { "Sec-Fetch-User", "?1" },
                    { "Sec-Fetch-Dest", "document" },
                    { "Referer", "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0" },
                    { "Accept-Encoding", "gzip, deflate, br" },
                    { "Accept-Language", "es,en;q=0.9" }
                };

                return HeadersLogin;
            }

            public static readonly Dictionary<string, string> HeadersAfterLogin = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Upgrade-Insecure-Requests", "1" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
            { "Sec-Fetch-Site", "same-origin" },
            { "Sec-Fetch-Mode", "navigate" },
            { "Sec-Fetch-Dest", "document" },
            { "Referer", "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es,en;q=0.9" }
        };

            public static Dictionary<string, string> GetHeadersPostAfterLogin(int contentLength)
            {
                Dictionary<string, string> HeadersPostAfterLogin = new Dictionary<string, string>() {
                    { "Host", "portalcfdi.facturaelectronica.sat.gob.mx" },
                    { "Connection", "keep-alive" },
                    { "Content-Length", contentLength.ToString() },
                    { "Cache-Control", "max-age=0" },
                    { "Upgrade-Insecure-Requests", "1" },
                    { "Origin", "https://cfdiau.sat.gob.mx" },
                    { "Content-Type", "application/x-www-form-urlencoded" },
                    { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
                    { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
                    { "Sec-Fetch-Site", "same-site" },
                    { "Sec-Fetch-Mode", "navigate" },
                    { "Sec-Fetch-Dest", "document" },
                    { "Referer", "https://cfdiau.sat.gob.mx/" },
                    { "Accept-Encoding", "gzip, deflate, br" },
                    { "Accept-Language", "es,en;q=0.9" }
                };

                return HeadersPostAfterLogin;

            }

            public static readonly Dictionary<string, string> HeadersPortalSat = new Dictionary<string, string>() {
            { "Host", "cfdiau.sat.gob.mx" },
            { "Connection", "keep-alive" },
            { "Cache-Control", "max-age=0" },
            { "Upgrade-Insecure-Requests", "1" },
             { "Origin", "https://cfdiau.sat.gob.mx" },
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" },
            { "Referer", "https://cfdiau.sat.gob.mx/" },
            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
            { "Accept-Encoding", "gzip, deflate, br" },
            { "Accept-Language", "es,en;q=0.9" }
        };

        }


    }
}
