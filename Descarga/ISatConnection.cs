﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public interface ISatConnection
    {
        System.Threading.Tasks.Task<string> GetCaptcha();
        System.Threading.Tasks.Task<bool> LoginAsync();
        System.Threading.Tasks.Task<string> DescargarArchivoXML(string cadenaDataXML, string fileSavePath);
        System.Threading.Tasks.Task<string> CerrarSesion();
        System.Threading.Tasks.Task<string> EmisorObtieneUsuario();
        System.Threading.Tasks.Task<string> ReceptorObtieneUsuario();
        System.Threading.Tasks.Task<string> MantieneSesionEmitidas(DateTime currentDateTime);
        System.Threading.Tasks.Task<string> VerificaSesionEmitidas(DateTime currentDateTime);

    }
}
