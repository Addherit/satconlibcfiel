﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class HttpUtils
    {
        public static Dictionary<string, string> GetFormData(string html)
        {
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(html);

            if (document == null)
                return null;

            Dictionary<string, string> post = new Dictionary<string, string>();
            List<string> searchTags = new List<string> { "input", "select" };

            var form = document.DocumentNode.GetElementsByTagName("form").FirstOrDefault();

            if (form == null)
                return null;

            foreach (var element in searchTags)
            {
                foreach (var val in form.GetElementsByTagName(element))
                {
                    var name = val.GetAttributeValue("name", string.Empty);

                    if (!string.IsNullOrEmpty(name))
                    {
                        post[name] = System.Net.WebUtility.HtmlDecode(val.GetAttributeValue("value", string.Empty));
                    }
                }
            }

            return post;
        }

    }
}
