﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class FlurlClientExtensions
    {
        public static IFlurlClient WithAutomaticDecompression(this IFlurlClient fc)
        {
            var httpClientHandler = new HttpClientHandler { AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate };
            //httpClientHandler.Proxy = new System.Net.WebProxy("localhost:8888", false);
            //httpClientHandler.UseProxy = true;
            fc.Settings.HttpClientFactory = new HttpClientFactoryWithWebRequestHandler(httpClientHandler);
            return fc;
        }
    }
}
