﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class URLS
    {
        public static string URL_CFDIAU1 = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        public static string URL_CFDIAU2 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0";
        public static string URL_CFDIAU3 = "https://cfdiau.sat.gob.mx/nidp/app/login?id=SATx509Custom&sid=0&option=credential&sid=0";
        public static string URL_CFDIAU4 = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        public static string URL_CONSULTA_RECEPTOR = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
        public static string URL_CONSULTA_EMISOR = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
        public static string URL_CONSULTA_RECEPTOR_OBTIENE_USUARIO = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx/ObtieneUsuario";
        public static string URL_CONSULTA_EMISOR_OBTIENE_USUARIO = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx/ObtieneUsuario";
        public static string URL_CERRAR_SESION = "https://cfdiau.sat.gob.mx/nidp/lofc.jsp";
        public static string URL_DESCARGA_XML = "https://portalcfdi.facturaelectronica.sat.gob.mx/RecuperaCfdi.aspx";
        public static string URL_DESCARGA_PDF = "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx";
        public static string URL_MANTIENE_SESION = "https://portalcfdi.facturaelectronica.sat.gob.mx/mantienesesion.aspx";
        public static string URL_VERIFICA_SESION = "https://portalcfdi.facturaelectronica.sat.gob.mx/verificasesion.aspx";
    }
    public static class CIEC_URLS
    {
        //CIEC
        public static string URL_CIEC_PORTAL = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        public static string URL_CIEC_LOGIN = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=0&option=credential&sid=0";
        public static string URL_CIEC_AFTER_LOGIN = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?sid=0";
    }
}
