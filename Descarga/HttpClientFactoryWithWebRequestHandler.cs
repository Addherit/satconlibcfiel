﻿using Flurl.Http.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class HttpClientFactoryWithWebRequestHandler : DefaultHttpClientFactory
    {
        private readonly HttpMessageHandler _httpMessageHandler;
        public HttpClientFactoryWithWebRequestHandler(HttpMessageHandler httpMessageHandler)
        {
            _httpMessageHandler = httpMessageHandler;
        }

        public override HttpMessageHandler CreateMessageHandler()
        {
            var handler = _httpMessageHandler;
            return handler;
        }
    }
}
