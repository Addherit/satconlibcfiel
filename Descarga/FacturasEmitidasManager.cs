﻿using Flurl.Http;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SATCONLIB
{
    public class FacturasEmitidasManager
    {
        private readonly IFlurlClient _client;
        private readonly ISatConnection _connection;
        private bool _loggedIn;
        private SynchronizedCollection<FacturaEmitidaMetadata> _cfdisDescargados;

        public FacturasEmitidasManager(ref IFlurlClient client, ref ISatConnection connection)
        {
            this._client = client;
            this._connection = connection;
            this._loggedIn = false;
            this._cfdisDescargados = new SynchronizedCollection<FacturaEmitidaMetadata>();
        }

        public bool IsLoggedIn()
        {
            return this._loggedIn;
        }

        public async System.Threading.Tasks.Task<string> Captcha()
        {
            try
            {
                await this._connection.CerrarSesion();
            }
            catch (Exception ex) { }

            string loginResult = await this._connection.GetCaptcha();
            //this._loggedIn = loginResult;
            return loginResult;
        }

        public async System.Threading.Tasks.Task<bool> LoginAsync()
        {
            try
            {
                await this._connection.CerrarSesion();
            }
            catch (Exception ex) { }

            bool loginResult = await this._connection.LoginAsync();
            this._loggedIn = loginResult;
            return this._loggedIn;
        }

        private async System.Threading.Tasks.Task<string> GetFacturasEmitidasPortalHtmlAsync()
        {
            var responseConsultaEmisor = await URLS.URL_CONSULTA_EMISOR.WithClient(this._client).WithHeaders(Headers.HeadersConsulta).GetAsync();
            var responseConsultaEmisorContent = await responseConsultaEmisor.GetStringAsync();
            return responseConsultaEmisorContent;
        }

        private List<KeyValuePair<string, string>> GetFormInputElements(string html)
        {
            var formParameters = new List<KeyValuePair<string, string>>();

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(html);

            var formElements = doc.GetElementsByTagName("form");

            HtmlNode form = null;

            if (formElements != null)
                form = formElements.FirstOrDefault();

            if (form != null)
            {

                foreach (HtmlNode node in form.GetElementsByTagName("input"))
                {
                    HtmlAttribute nameAttribute = node.Attributes["name"];
                    HtmlAttribute valueAttribute = node.Attributes["value"];

                    if (nameAttribute != null && valueAttribute != null)
                    {
                        formParameters.Add(new KeyValuePair<string, string>(nameAttribute.Value, valueAttribute.Value));
                    }
                }
            }

            return formParameters;
        }

        private async System.Threading.Tasks.Task<string> GetFacturasEmitidasSolicitudPrevia(string htmlFacturasEmitidasPortal)
        {
            var formInputElements = GetFormInputElements(htmlFacturasEmitidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$RdoFechas"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", "ctl00$MainContent$RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            var viewState = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATE"); ;
            if (!viewState.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(viewState.Key, viewState.Value));
            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;
            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicial", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFinal", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDatos", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFlag", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfAux", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfCancelacion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfUrlDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hdnValAccion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfXML", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));

            var responseConsultaEmisor = await URLS.URL_CONSULTA_EMISOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedEmitidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();
            return responseConsultaEmisor;
        }

        public async System.Threading.Tasks.Task ObtenerListaDeFechasDeDescarga(DateTime PrimerFecha, DateTime SegundaFecha)
        {

            Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>>
            {
                Value = new SynchronizedCollection<Tuple<DateTime, DateTime>>()
            };

            await ObtenerCantidadDeFacturas(IntervalTimeList, PrimerFecha, SegundaFecha);
        }

        public async Task ObtenerCantidadDeFacturas(Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> intervalTimeList, DateTime fechaInicial, DateTime fechaFinal)
        {
            Console.Write("\rSolicitando datos de fecha de {0} a {1}...", fechaInicial.ToString("dd/MM/yyyy HH:mm:ss"), fechaFinal.ToString("dd/MM/yyyy HH:mm:ss"));
            string responseGetFacturasEmitidas = await GetFacturasEmitidasEnRangoDeFecha(fechaInicial, fechaFinal);
            bool respuestaExitosa = responseGetFacturasEmitidas.Contains("|updatePanel|ctl00_MainContent_UpnlBusqueda|");

            if (!respuestaExitosa)
            {
                await ObtenerCantidadDeFacturas(intervalTimeList, fechaInicial, fechaFinal);
            }

            bool masDeQuinientos = responseGetFacturasEmitidas.Contains("ctl00_MainContent_PnlLimiteRegistros");

            if (!masDeQuinientos)
            {
                intervalTimeList.Value.Add(new Tuple<DateTime, DateTime>(fechaInicial, fechaFinal));
                return;
            }

            long HalfTimeSpan = (fechaFinal.Ticks - fechaInicial.Ticks) / 2;
            TimeSpan FirstTimeSpanHalf = new TimeSpan(fechaInicial.Ticks + HalfTimeSpan);
            DateTime FirstTimeHalf = DateTime.MinValue + FirstTimeSpanHalf;
            TimeSpan SecondTimeSpanHalf = new TimeSpan(fechaFinal.Ticks - HalfTimeSpan);
            DateTime SecondTimeHalf = (DateTime.MinValue + SecondTimeSpanHalf).AddSeconds(1);
            await ObtenerCantidadDeFacturas(intervalTimeList, fechaInicial, FirstTimeHalf);
            await ObtenerCantidadDeFacturas(intervalTimeList, SecondTimeHalf, fechaFinal);
        }

        public async System.Threading.Tasks.Task<string> GetFacturasEmitidasEnRangoDeFecha(DateTime fechaInicial, DateTime fechaFinal)
        {
            var htmlFacturasEmitidasPortal = await GetFacturasEmitidasPortalHtmlAsync();
            //await ObtenerUsuarioEmitidas();
            var htmlConsultaPrevia = await GetFacturasEmitidasSolicitudPrevia(htmlFacturasEmitidasPortal);
            //await ObtenerUsuarioEmitidas();
            var formInputElements = GetFormInputElements(htmlFacturasEmitidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();
            await this._connection.MantieneSesionEmitidas(DateTime.Now);

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$BtnBusqueda"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFechas"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicial", fechaInicial.Year.ToString())); // EL anyo de la fecha inicial?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "false"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$Calendario_text", fechaInicial.ToString("dd/MM/yyyy")));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlHora", fechaInicial.Hour.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlMinuto", fechaInicial.Minute.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlSegundo", fechaInicial.Second.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFinal", "")); // El anyo de la fecha final?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$Calendario_text", fechaFinal.ToString("dd/MM/yyyy")));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlHora", fechaFinal.Hour.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlMinuto", fechaFinal.Minute.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlSegundo", fechaFinal.Second.ToString()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDatos", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFlag", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfAux", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfCancelacion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfUrlDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hdnValAccion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfXML", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            string viewStatePattern = @"__VIEWSTATE\|(.*?)\|";
            Regex viewStateRegex = new Regex(viewStatePattern);
            Match viewStateMatch = viewStateRegex.Match(htmlConsultaPrevia);
            if (viewStateMatch.Success)
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATE", viewStateMatch.Groups[1].Value));

            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;

            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));

            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$BtnBusqueda", "Buscar CFDI"));

            var responseConsultaEmisor = await URLS.URL_CONSULTA_EMISOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedEmitidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();

            return responseConsultaEmisor;
        }

        public async System.Threading.Tasks.Task<string> GetFacturasEmitidasPorFolioFiscal(string htmlConsultaPrevia, string folioFiscal)
        {
            var htmlFacturasEmitidasPortal = await GetFacturasEmitidasPortalHtmlAsync();
            var formInputElements = GetFormInputElements(htmlFacturasEmitidasPortal);
            var queryParameters = new List<KeyValuePair<string, string>>();

            //Empezamos obteniendo los valores de los parametros.
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$ScriptManager1", "ctl00$MainContent$UpnlBusqueda|ctl00$MainContent$BtnBusqueda"));
            var csrfToken = formInputElements.SingleOrDefault(x => x.Key == "__CSRFTOKEN");
            if (!csrfToken.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>(csrfToken.Key, csrfToken.Value));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTTARGET", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__EVENTARGUMENT", ""));
            queryParameters.Add(new KeyValuePair<string, string>("__LASTFOCUS", ""));
            string viewStatePattern = @"__VIEWSTATE\|(.*?)\|";
            Regex viewStateRegex = new Regex(viewStatePattern);
            Match viewStateMatch = viewStateRegex.Match(htmlConsultaPrevia);
            if (viewStateMatch.Success)
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATE", viewStateMatch.Groups[1].Value));

            var viewStateGenerator = formInputElements.SingleOrDefault(x => x.Key == "__VIEWSTATEGENERATOR"); ;

            if (!viewStateGenerator.Equals(new KeyValuePair<string, string>()))
                queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEGENERATOR", viewStateGenerator.Value));

            queryParameters.Add(new KeyValuePair<string, string>("__VIEWSTATEENCRYPTED", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$FiltroCentral", "RdoFolioFiscal"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtUUID", folioFiscal.ToUpper()));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicial", "")); // EL anyo de la fecha inicial?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfInicialBool", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaInicial2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFinal", "")); // El anyo de la fecha final?
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$Calendario_text", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlHora", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlMinuto", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$CldFechaFinal2$DdlSegundo", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$TxtRfcReceptor", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$DdlEstadoComprobante", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlComplementos", "-1"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlVigente", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$ddlCancelado", "0"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDatos", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfFlag", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfAux", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfCancelacion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfUrlDescarga", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfParametrosMetadata", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hdnValAccion", ""));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$hfXML", ""));




            queryParameters.Add(new KeyValuePair<string, string>("__ASYNCPOST", "true"));
            queryParameters.Add(new KeyValuePair<string, string>("ctl00$MainContent$BtnBusqueda", "Buscar CFDI"));

            var responseConsultaEmisor = await URLS.URL_CONSULTA_EMISOR.WithClient(this._client).WithHeaders(Headers.HeadersConsultaXRequestedEmitidas).PostAsync(new FormUrlEncodedContent(queryParameters)).ReceiveString();

            return responseConsultaEmisor;
        }

        public async System.Threading.Tasks.Task<string> CerrarSesionAsync()
        {
            return await this._connection.CerrarSesion();
        }

        public async System.Threading.Tasks.Task<string> DescargarArchivoXML(FacturaEmitidaMetadata facturaEmitida, string fileSavePath)
        {
            string downloadPath = string.Empty;
            if (!string.IsNullOrEmpty(facturaEmitida.XmlDataDescarga))
            {
                downloadPath = await this._connection.DescargarArchivoXML(facturaEmitida.XmlDataDescarga, fileSavePath);
            }
            return downloadPath;
        }

        public async System.Threading.Tasks.Task<string> ObtenerUsuarioEmitidas()
        {
            var responseEmisorObtieneUsuario = await URLS.URL_CONSULTA_EMISOR_OBTIENE_USUARIO.WithClient(this._client).WithHeaders(Headers.HeadersObtieneUsuarioEmisor).PostAsync(null);
            var responseEmisorObtieneUsuarioContent = await responseEmisorObtieneUsuario.GetStringAsync();
            return responseEmisorObtieneUsuarioContent;
        }

        public List<string> ObtenerCfdisDescargadosEnRangoDeFechas(DateTime fechaInicial, DateTime fechaFinal, string emisorRFC)
        {
            using (FacturasDescargadasEntities ctx = new FacturasDescargadasEntities(ConnectionData.ConnectionString, ConnectionData.Schema))
            {
                ctx.Database.CommandTimeout = 36000;
                List<string> ListaFoliosFiscalesDescargados = new List<string>();
                var ListaDeFacturasEmitidas = ctx.Comprobantes.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal && x.Emisor.Rfc.ToUpper() == emisorRFC.ToUpper());
                foreach (var cfdi in ListaDeFacturasEmitidas)
                {
                    ListaFoliosFiscalesDescargados.Add(cfdi.Complemento.TimbreFiscalDigital.UUID.ToString().ToUpper());
                }
                return ListaFoliosFiscalesDescargados;
            }
        }

        public SynchronizedCollection<FacturaEmitidaMetadata> ObtenerCFDISDescargadosEnSesion()
        {
            return this._cfdisDescargados;
        }

        public async System.Threading.Tasks.Task ObtenerNumeroFacturasEnFecha(DateTime PrimerFecha, DateTime SegundaFecha)
        {

            Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>>
            {
                Value = new SynchronizedCollection<Tuple<DateTime, DateTime>>()
            };

            await ObtenerCantidadDeFacturas(IntervalTimeList, PrimerFecha, SegundaFecha);
        }

        public async Task ObtenerNumeroFacturasDia(Ref<Tuple<DateTime, DateTime>> dia, Ref<SynchronizedCollection<FacturaEmitidaMetadata>> facturaCollection)
        {
            DateTime diaFechaInicial = dia.Value.Item1;
            DateTime diaFechaFinal = dia.Value.Item2;
            Console.WriteLine("\nObteniendo cantidad de facturas");

            Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>> IntervalTimeList = new Ref<SynchronizedCollection<Tuple<DateTime, DateTime>>>
            {
                Value = new SynchronizedCollection<Tuple<DateTime, DateTime>>()
            };

            await ObtenerCantidadDeFacturas(IntervalTimeList, diaFechaInicial, diaFechaFinal);

            foreach (var periodo in IntervalTimeList.Value)
            {
                var facturasEmitidasEnRango = await GetFacturasEmitidasEnRangoDeFecha(periodo.Item1, periodo.Item2);
                var facturasEmitidasLista = FacturasEmitidasParser.GetListaFacturasEmitidas(facturasEmitidasEnRango);

                foreach (var factura in facturasEmitidasLista)
                {
                    facturaCollection.Value.Add(factura);
                }
            }
        }
                
    }
}
