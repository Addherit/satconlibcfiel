﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class FacturaRecibidaMetadata
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [NotMapped]
        public string XmlDataDescarga { get; set; }
        [NotMapped]
        public string PdfDataDescarga { get; set; }
        public Guid FolioFiscal { get; set; }
        public string RfcEmisor { get; set; }
        public string RazonSocialEmisor { get; set; }
        public string RfcReceptor { get; set; }
        public string RazonSocialReceptor { get; set; }
        public DateTime FechaDeEmision { get; set; }
        public DateTime FechaDeCertificacion { get; set; }
        public string PacQueCertifico { get; set; }
        public decimal Total { get; set; }
        public string EfectoDelComprobante { get; set; }
        public string EstatusDeCancelacion { get; set; }
        public string EstadoDelComprobante { get; set; }
        public string EstatusDeProcesoDeCancelacion { get; set; }
        public DateTime? FechaDeProcesoDeCancelacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
    }
}
