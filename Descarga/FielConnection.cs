﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using HtmlAgilityPack;
using System.Security.Cryptography;
using System.Globalization;
using System.Net.Http;
using Flurl.Http.Configuration;

namespace SATCONLIB
{
    public class FIELConnection : ISatConnection
    {
        private X509Certificate2 cert;
        private IFlurlClient client;


        public FIELConnection(X509Certificate2 _cert, ref IFlurlClient _client)//X509Certificate2 cert)
        {
            this.cert = _cert;
            this.client = _client;
        }

        public async System.Threading.Tasks.Task<bool> LoginAsync()
        {

            try
            {
                var response1 = await URLS.URL_CFDIAU1.WithClient(this.client).WithHeaders(Headers.HeadersPortal1).GetStringAsync();
                var response2 = await URLS.URL_CFDIAU2.WithClient(this.client).WithHeaders(Headers.HeadersPortal1).PostStringAsync("");
                var response3 = await URLS.URL_CFDIAU3.WithClient(client).WithHeaders(Headers.HeadersPortal3).GetStringAsync();

                //Formamos el token
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(response3);

                if (document == null)
                    throw new Exception("No se puede cargar la pagina de inicio de sesión FIEL en el portal del SAT.");

                Dictionary<string, string> post = new Dictionary<string, string>();
                List<string> searchTags = new List<string> { "input", "select" };

                var form = document.GetElementbyId("certform");

                foreach (var element in searchTags)
                {
                    foreach (var val in form.GetElementsByTagName(element))
                    {
                        var name = val.GetAttributeValue("name", string.Empty);

                        if (!string.IsNullOrEmpty(name))
                        {
                            post[name] = val.GetAttributeValue("value", string.Empty);
                        }
                    }
                }

                if (post.Count == 0)
                    throw new Exception("No se pueden obtener los elementos del formulario FIEL en la pagina del SAT.");

                var guid = post["guid"];
                //var guid = "NzRkYjk0ZTktNzVjNi00Y2I1LTk2ZWEtM2U1MWE5YzE2ZGM4";
                var serie = Encoding.ASCII.GetString(cert.GetSerialNumber().Reverse().ToArray());
                var rfc = string.Empty;

                string[] subject = cert.Subject.Split(',');
                foreach (string strVal in subject)
                {
                    string value = strVal.Trim();
                    if (value.StartsWith("OID.2.5.4.45="))
                    {
                        string value2 = value.Replace("OID.2.5.4.45=", "");
                        rfc = value2.Substring(0, value2.IndexOf('/') >= 0 ? value2.IndexOf('/') : value2.Length).Trim();
                    }
                }

                string[] rangoValidez = new string[2];
                rangoValidez[0] = cert.GetEffectiveDateString();
                rangoValidez[1] = cert.GetExpirationDateString();

                var co = guid + "|" + rfc + "|" + serie;
                var firma = string.Empty;

                var certificado = Convert.ToBase64String(cert.GetRawCertData());

                byte[] signature = null;

                using (RSA rsaCryptoServiceProvider = cert.GetRSAPrivateKey())
                {
                    signature = rsaCryptoServiceProvider.SignData(Encoding.UTF8.GetBytes(co), HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                }

                firma = Convert.ToBase64String(signature);

                var co_encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(co));
                var firma_encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(firma));

                var firma_completa = co_encoded + "#" + firma_encoded;

                post["token"] = Convert.ToBase64String(Encoding.UTF8.GetBytes(firma_completa));

                DateTime validoHasta;
                CultureInfo culture = CultureInfo.CurrentCulture;
                DateTimeStyles styles = DateTimeStyles.None;
                DateTime.TryParse(rangoValidez[1], culture, styles, out validoHasta);

                post["fert"] = validoHasta.ToUniversalTime().ToString("yyMMddHHmmssZ");

                var response4 = await URLS.URL_CFDIAU3.WithClient(client).WithHeaders(Headers.HeadersPortal4).PostUrlEncodedAsync(post);
                var response4_content = await response4.GetStringAsync();
                var form_response4 = HttpUtils.GetFormData(response4_content);

                var response5 = await URLS.URL_CFDIAU4.WithClient(client).WithHeaders(Headers.HeadersPortal5).PostUrlEncodedAsync(form_response4);
                var response5Content = await response5.GetStringAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async System.Threading.Tasks.Task<string> ReceptorObtieneUsuario()
        {
            var response_obtiene_usuario = await URLS.URL_CONSULTA_RECEPTOR.WithClient(client).WithHeaders(Headers.HeadersObtieneUsuarioReceptor).PostStringAsync(string.Empty);
            return await response_obtiene_usuario.GetStringAsync();
        }

        public async System.Threading.Tasks.Task<string> EmisorObtieneUsuario()
        {
            var response_obtiene_usuario = await URLS.URL_CONSULTA_EMISOR.WithClient(client).WithHeaders(Headers.HeadersObtieneUsuarioEmisor).PostStringAsync(string.Empty);
            return await response_obtiene_usuario.GetStringAsync();
        }

        public async System.Threading.Tasks.Task<string> MantieneSesionEmitidas(DateTime currentDateTime)
        {
            long unixTimeMilliseconds = (long)(currentDateTime - new DateTime(1970, 1, 1)).TotalMilliseconds;
            var response_mantiene_sesion = await URLS.URL_MANTIENE_SESION.WithClient(client).WithHeaders(Headers.HeadersConsultaXRequestedMantieneSesion).SetQueryParam("_", unixTimeMilliseconds.ToString()).GetStringAsync();
            return response_mantiene_sesion;
        }

        public async System.Threading.Tasks.Task<string> VerificaSesionEmitidas(DateTime currentDateTime)
        {
            long unixTimeMilliseconds = (long)(currentDateTime - new DateTime(1970, 1, 1)).TotalMilliseconds;
            var response_verifica_sesion = await URLS.URL_VERIFICA_SESION.WithClient(client).WithHeaders(Headers.HeadersConsultaXRequestedMantieneSesion).SetQueryParam("_", unixTimeMilliseconds.ToString()).GetStringAsync();
            return response_verifica_sesion;
        }

        public async System.Threading.Tasks.Task<string> CerrarSesion()
        {
            var response_cerrar_sesion = await URLS.URL_CERRAR_SESION.WithClient(client).WithHeaders(Headers.HeadersCerrarSesion).PostStringAsync(string.Empty);
            return await response_cerrar_sesion.GetStringAsync();
        }

        public async System.Threading.Tasks.Task<string> ObtenerMetadatosFacturasRecibidas(DateTime FechaInicial, DateTime FechaFinal)
        {
            var responseConsultaEmisor = await URLS.URL_CONSULTA_EMISOR.WithClient(client).WithHeaders(Headers.HeadersConsulta).GetAsync();
            var responseConsultaEmisorContent = await responseConsultaEmisor.GetStringAsync();
            return responseConsultaEmisorContent;
        }

        public async System.Threading.Tasks.Task<string> DescargarArchivoXML(string cadenaXMLData, string fileSavePath)
        {
            var responseDescargaArchivo = await URLS.URL_DESCARGA_XML.WithClient(client).WithHeaders(Headers.HeadersDescargaEmitidos).SetQueryParam("Datos", cadenaXMLData).DownloadFileAsync(fileSavePath);
            return responseDescargaArchivo;
        }

        public Task<string> GetCaptcha()
        {
            throw new NotImplementedException();
        }
    }
}
