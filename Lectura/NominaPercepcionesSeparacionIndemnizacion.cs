﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SATCONLIB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/nomina12")]
    public partial class NominaPercepcionesSeparacionIndemnizacion
    {
        private Guid idField;

        private decimal totalPagadoField;

        private int numAñosServicioField;

        private decimal ultimoSueldoMensOrdField;

        private decimal ingresoAcumulableField;

        private decimal ingresoNoAcumulableField;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalPagado
        {
            get
            {
                return this.totalPagadoField;
            }
            set
            {
                this.totalPagadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int NumAñosServicio
        {
            get
            {
                return this.numAñosServicioField;
            }
            set
            {
                this.numAñosServicioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal UltimoSueldoMensOrd
        {
            get
            {
                return this.ultimoSueldoMensOrdField;
            }
            set
            {
                this.ultimoSueldoMensOrdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal IngresoAcumulable
        {
            get
            {
                return this.ingresoAcumulableField;
            }
            set
            {
                this.ingresoAcumulableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal IngresoNoAcumulable
        {
            get
            {
                return this.ingresoNoAcumulableField;
            }
            set
            {
                this.ingresoNoAcumulableField = value;
            }
        }
    }
}
