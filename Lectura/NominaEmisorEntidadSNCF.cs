﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SATCONLIB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/nomina12")]
    public partial class NominaEmisorEntidadSNCF
    {

        private Guid idField;

        private string origenRecursoField;

        private decimal montoRecursoPropioField;

        private bool montoRecursoPropioFieldSpecified;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OrigenRecurso
        {
            get
            {
                return this.origenRecursoField;
            }
            set
            {
                this.origenRecursoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoRecursoPropio
        {
            get
            {
                return this.montoRecursoPropioField;
            }
            set
            {
                this.montoRecursoPropioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MontoRecursoPropioSpecified
        {
            get
            {
                return this.montoRecursoPropioFieldSpecified;
            }
            set
            {
                this.montoRecursoPropioFieldSpecified = value;
            }
        }
    }
}
