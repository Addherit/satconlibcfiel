﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SATCONLIB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/nomina12")]
    public partial class NominaPercepciones
    {
        private Guid idField;

        private List<NominaPercepcionesPercepcion> percepcionField;

        private NominaPercepcionesJubilacionPensionRetiro jubilacionPensionRetiroField;

        private NominaPercepcionesSeparacionIndemnizacion separacionIndemnizacionField;

        private decimal totalSueldosField;

        private bool totalSueldosFieldSpecified;

        private decimal totalSeparacionIndemnizacionField;

        private bool totalSeparacionIndemnizacionFieldSpecified;

        private decimal totalJubilacionPensionRetiroField;

        private bool totalJubilacionPensionRetiroFieldSpecified;

        private decimal totalGravadoField;

        private decimal totalExentoField;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Percepcion")]
        public virtual List<NominaPercepcionesPercepcion> Percepcion
        {
            get
            {
                return this.percepcionField;
            }
            set
            {
                this.percepcionField = value;
            }
        }

        /// <remarks/>
        public NominaPercepcionesJubilacionPensionRetiro JubilacionPensionRetiro
        {
            get
            {
                return this.jubilacionPensionRetiroField;
            }
            set
            {
                this.jubilacionPensionRetiroField = value;
            }
        }

        /// <remarks/>
        public NominaPercepcionesSeparacionIndemnizacion SeparacionIndemnizacion
        {
            get
            {
                return this.separacionIndemnizacionField;
            }
            set
            {
                this.separacionIndemnizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalSueldos
        {
            get
            {
                return this.totalSueldosField;
            }
            set
            {
                this.totalSueldosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalSueldosSpecified
        {
            get
            {
                return this.totalSueldosFieldSpecified;
            }
            set
            {
                this.totalSueldosFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalSeparacionIndemnizacion
        {
            get
            {
                return this.totalSeparacionIndemnizacionField;
            }
            set
            {
                this.totalSeparacionIndemnizacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalSeparacionIndemnizacionSpecified
        {
            get
            {
                return this.totalSeparacionIndemnizacionFieldSpecified;
            }
            set
            {
                this.totalSeparacionIndemnizacionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalJubilacionPensionRetiro
        {
            get
            {
                return this.totalJubilacionPensionRetiroField;
            }
            set
            {
                this.totalJubilacionPensionRetiroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalJubilacionPensionRetiroSpecified
        {
            get
            {
                return this.totalJubilacionPensionRetiroFieldSpecified;
            }
            set
            {
                this.totalJubilacionPensionRetiroFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalGravado
        {
            get
            {
                return this.totalGravadoField;
            }
            set
            {
                this.totalGravadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalExento
        {
            get
            {
                return this.totalExentoField;
            }
            set
            {
                this.totalExentoField = value;
            }
        }
    }
}
