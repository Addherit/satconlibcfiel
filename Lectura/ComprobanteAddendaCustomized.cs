﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SATCONLIB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/3")]
    public partial class ComprobanteAddendaCustomized
    {
        private Guid idField;

        private string ticketNumberField;

        private string iATAAgencyCodeField;

        private string pNRField;

        private List<ComprobanteAddendaCustomizedAdditionalInformation> additionalInformationField;

        private List<ComprobanteAddendaCustomizedDatosReceptorDireccion> datosReceptorField;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string IATAAgencyCode
        {
            get
            {
                return this.iATAAgencyCodeField;
            }
            set
            {
                this.iATAAgencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string PNR
        {
            get
            {
                return this.pNRField;
            }
            set
            {
                this.pNRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AdditionalInformation", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public virtual List<ComprobanteAddendaCustomizedAdditionalInformation> AdditionalInformation
        {
            get
            {
                return this.additionalInformationField;
            }
            set
            {
                this.additionalInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("Direccion", typeof(ComprobanteAddendaCustomizedDatosReceptorDireccion), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public virtual List<ComprobanteAddendaCustomizedDatosReceptorDireccion> DatosReceptor
        {
            get
            {
                return this.datosReceptorField;
            }
            set
            {
                this.datosReceptorField = value;
            }
        }
    }
}
