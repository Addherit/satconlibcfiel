﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SATCONLIB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/nomina12")]
    public partial class NominaPercepcionesJubilacionPensionRetiro
    {

        private Guid idField;

        private decimal totalUnaExhibicionField;

        private bool totalUnaExhibicionFieldSpecified;

        private decimal totalParcialidadField;

        private bool totalParcialidadFieldSpecified;

        private decimal montoDiarioField;

        private bool montoDiarioFieldSpecified;

        private decimal ingresoAcumulableField;

        private decimal ingresoNoAcumulableField;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalUnaExhibicion
        {
            get
            {
                return this.totalUnaExhibicionField;
            }
            set
            {
                this.totalUnaExhibicionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalUnaExhibicionSpecified
        {
            get
            {
                return this.totalUnaExhibicionFieldSpecified;
            }
            set
            {
                this.totalUnaExhibicionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal TotalParcialidad
        {
            get
            {
                return this.totalParcialidadField;
            }
            set
            {
                this.totalParcialidadField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalParcialidadSpecified
        {
            get
            {
                return this.totalParcialidadFieldSpecified;
            }
            set
            {
                this.totalParcialidadFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoDiario
        {
            get
            {
                return this.montoDiarioField;
            }
            set
            {
                this.montoDiarioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MontoDiarioSpecified
        {
            get
            {
                return this.montoDiarioFieldSpecified;
            }
            set
            {
                this.montoDiarioFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal IngresoAcumulable
        {
            get
            {
                return this.ingresoAcumulableField;
            }
            set
            {
                this.ingresoAcumulableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal IngresoNoAcumulable
        {
            get
            {
                return this.ingresoNoAcumulableField;
            }
            set
            {
                this.ingresoNoAcumulableField = value;
            }
        }
    }
}
