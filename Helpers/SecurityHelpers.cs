﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class SecurityHelpers
    {
        /// <summary>
        /// Convierte un CER y un KEY en un PFX
        /// </summary>
        /// <param name="cerText">Certificado CER en base64</param>
        /// <param name="keyText">llave privada KEY en base64</param>
        /// <param name="keyPass">password de la llaveprivada</param>
        /// <returns>el certificado pfx</returns>
        public static byte[] CerKey2Pfx(string cerText, string keyText, string keyPass)
        {
            try
            {
                byte[] pfxBytes = null;

                var certificado = new System.Security.Cryptography.X509Certificates.X509Certificate2(Convert.FromBase64String(cerText));
                var pllavePrivada = Org.BouncyCastle.Security.PrivateKeyFactory.DecryptKey(keyPass.ToCharArray(), Convert.FromBase64String(keyText));

                var certificadoBC = new Org.BouncyCastle.X509.X509Certificate(Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(certificado).CertificateStructure);
                var acertificadoBC = new Org.BouncyCastle.Pkcs.X509CertificateEntry[1];
                acertificadoBC[0] = new Org.BouncyCastle.Pkcs.X509CertificateEntry(certificadoBC);

                var pfxStore = new Org.BouncyCastle.Pkcs.Pkcs12Store();
                pfxStore.SetKeyEntry("", new Org.BouncyCastle.Pkcs.AsymmetricKeyEntry(pllavePrivada), acertificadoBC);
                using (var pfxStream = new MemoryStream())
                {
                    pfxStore.Save(pfxStream, keyPass.ToCharArray(), new Org.BouncyCastle.Security.SecureRandom());
                    pfxBytes = pfxStream.ToArray();
                }



                var resultado = Org.BouncyCastle.Pkcs.Pkcs12Utilities.ConvertToDefiniteLength(pfxBytes, keyPass.ToCharArray());

                return resultado;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
