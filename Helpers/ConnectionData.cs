﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public static class ConnectionData
    {
        public static string Schema { get; set; }
        public static string ConnectionString { get; set; }
    }
}
