﻿using Flurl.Http;
using Polly;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Net.Security;
using System.Linq;

namespace SATCONLIB
{
    class Program
    {
        private static readonly int MAX_LOGIN_RETRIES = 10;
        private static readonly int RETRY_WAIT_TIME = 20;
        private static string DescargaRaiz = "E:\\Addon xml\\";
        private static string DirDescarga = "";
        //Obtencion del dia
        //private static DateTime dd = DateTime.Now.AddDays(-5);
        private static DateTime dd1 = DateTime.Now;
        private static string d = dd1.ToString("dd-MM-yyyy")+" 00:00:00";
        private static string d2 = dd1.ToString("dd-MM-yyyy HH:mm:ss");
        private static readonly DateTime CONSULTA_PRIMERA_FECHA = DateTime.ParseExact("25-03-2022 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.CurrentCulture);
        private static readonly DateTime CONSULTA_SEGUNDA_FECHA = DateTime.ParseExact("31-03-2022 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.CurrentCulture);
        //private static readonly DateTime CONSULTA_PRIMERA_FECHA = DateTime.ParseExact(d, "dd-MM-yyyy HH:mm:ss", CultureInfo.CurrentCulture);
        //private static readonly DateTime CONSULTA_SEGUNDA_FECHA = DateTime.ParseExact(d2, "dd-MM-yyyy HH:mm:ss", CultureInfo.CurrentCulture);

        static async Task Main(string[] args)
        {
          
            Console.WriteLine(CONSULTA_PRIMERA_FECHA);
            Console.WriteLine(CONSULTA_SEGUNDA_FECHA);
            string TipoAcceso = "FIEL";
            string TipoConsulta = "TODOS";
            string descarga = "tj";
            //extraccion fechas
            string Anio = string.Format(string.Format("{0:0000}", Convert.ToInt32(CONSULTA_PRIMERA_FECHA.Year)));
            string Mes = string.Format(string.Format("{0:00}", Convert.ToInt32(CONSULTA_PRIMERA_FECHA.Month)));


            RFCData DatosRFC = new RFCData();
            DatosRFC.Id = 1;

            switch (descarga)
            {
                case "tj":
                    {
                        DatosRFC.Rfc = "TLE041108QP8";
                        DatosRFC.RazonSocial = "Troglo de León SA de CV";
                    }
                    break;
                case "parque":
                    {
                        DatosRFC.Rfc = "MPA140527UB9";
                        DatosRFC.RazonSocial = "Manufacturas PARQE SA de CV";
                    }
                    break;
                case "noble":
                    {
                        DatosRFC.Rfc = "NAJ070821RX4";
                        DatosRFC.RazonSocial = "Noble & Jase sistemas de iluminación SA de CV";
                    }
                    break;
                case "old":
                    {
                        DatosRFC.Rfc = "OCO080911BW8";
                        DatosRFC.RazonSocial = "Old Construcciones";
                    }
                    break;
                case "meqsa":
                    {
                        DatosRFC.Rfc = "MQU090428812";
                        DatosRFC.RazonSocial = "MEGAELECTRICA DE QUERETARO, S.A. DE C.V.";
                    }
                    break;
                case "eypo":
                    {
                        DatosRFC.Rfc = "EPO020828E5A";
                        DatosRFC.RazonSocial = "Electro Luminacion Y Proyectos de Occidente";
                    }
                    break;
            }
            


            DatosRFC.PasswordClaveCIEC = "";
            string DirWork = Anio + "\\" + DatosRFC.RazonSocial + "\\" + TipoConsulta + "\\"+Mes+"\\";
            DirDescarga = DescargaRaiz + DirWork;

            //////////////////////////////////////////////////////////////////////////////////////////
            #region PARAMETROS POR ACCESO FIEL

            string certFilePath = "";
            string privateKeyFilePath = "";
            string privateKeyPassword = "";

            switch (descarga)
            {
                case "tj":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\TLE041108QP8\\00001000000509805247.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\TLE041108QP8\\Claveprivada_FIEL_TLE041108QP8_20211109_093725.key";
                         privateKeyPassword = "Tl010730";
                    }
                    break;
                case "parque":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\MPA140527UB9\\mpa140527ub9.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\MPA140527UB9\\Claveprivada_FIEL_MPA140527UB9_20180801_091438.key";
                         privateKeyPassword = "Mp010730";
                    }
                    break;
                case "noble":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\NAJ070821RX4\\00001000000510877261.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\NAJ070821RX4\\Claveprivada_FIEL_NAJ070821RX4_20220113_145245.key";
                         privateKeyPassword = "Na010730";
                    }
                    break;
                case "old":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\OCO080911BW8\\00001000000510876589.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\OCO080911BW8\\Claveprivada_FIEL_OCO080911BW8_20220113_143713.key";
                         privateKeyPassword = "Oc010730";
                    }
                    break;
                case "meqsa":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\MQU090428812\\00001000000504234899.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\MQU090428812\\Claveprivada_FIEL_MQU090428812_20200618_100605.key";
                         privateKeyPassword = "Mq010730";
                    }
                    break;
                case "eypo":
                    {
                         certFilePath = @"E:\\Addon xml\\Certificados\\EPO020828E5A\\00001000000508067642.cer";
                         privateKeyFilePath = @"E:\\Addon xml\\Certificados\\EPO020828E5A\\Claveprivada_FIEL_EPO020828E5A_20210706_110328.key";
                         privateKeyPassword = "Ep010730";
                    }
                    break;
            }


            DatosRFC.Certificado = File.ReadAllBytes(certFilePath);
            DatosRFC.ClavePrivada = File.ReadAllBytes(privateKeyFilePath);
            DatosRFC.PasswordClavePrivada = privateKeyPassword;

            string RFC_FIEL_CERTIFICATE = Convert.ToBase64String(DatosRFC.Certificado);
            string RFC_FIEL_PRIVATE_KEY = Convert.ToBase64String(DatosRFC.ClavePrivada);
            string RFC_FIEL_PASSWORD_PRIVATE_KEY = DatosRFC.PasswordClavePrivada;

            #endregion PARAMETROS POR ACCESO FIEL
            ////////////////////////////////////////////////////////////////////////////////////


            Console.WriteLine(String.Format("Procesando RFC: {0}", DatosRFC.Rfc));
            ConnectionData.Schema = DatosRFC.Rfc.ToUpper();

            switch (TipoConsulta)
            {
                case "EMITIDOS":
                    {
                        if (!Directory.Exists(DirDescarga))
                            Directory.CreateDirectory(DirDescarga);


                        bool consultaFacturasEmitidasResult = false;
                        if (TipoAcceso == "FIEL")
                        {
                            while (!consultaFacturasEmitidasResult)
                                consultaFacturasEmitidasResult = await ConsultarFacturasEmitidasFIEL(RFC_FIEL_CERTIFICATE, RFC_FIEL_PRIVATE_KEY, RFC_FIEL_PASSWORD_PRIVATE_KEY, false);
                        }
                    }
                    break;

                case "RECIBIDOS":
                    {
                        if (!Directory.Exists(DescargaRaiz + DirWork))
                            Directory.CreateDirectory(DescargaRaiz + DirWork);

                        bool consultaFacturasRecibidasResult = false;


                        if (TipoAcceso == "FIEL")
                        {
                            while (!consultaFacturasRecibidasResult)
                                consultaFacturasRecibidasResult = await ConsultarFacturasRecibidasFIEL(RFC_FIEL_CERTIFICATE, RFC_FIEL_PRIVATE_KEY, RFC_FIEL_PASSWORD_PRIVATE_KEY, false);
                        }
                    }
                    break;

                case "TODOS":
                    {
                        //Emitidos
                        DirWork = Anio + "\\" + DatosRFC.RazonSocial + "\\EMITIDOS\\" + Mes + "\\";
                        DirDescarga = DescargaRaiz + DirWork;

                        if (!Directory.Exists(DirDescarga))
                            Directory.CreateDirectory(DirDescarga);


                        bool consultaFacturasEmitidasResult = false;
                        if (TipoAcceso == "FIEL")
                        {
                            while (!consultaFacturasEmitidasResult)
                                consultaFacturasEmitidasResult = await ConsultarFacturasEmitidasFIEL(RFC_FIEL_CERTIFICATE, RFC_FIEL_PRIVATE_KEY, RFC_FIEL_PASSWORD_PRIVATE_KEY, false);
                        }

                        //Recibidos
                        DirWork = Anio + "\\" + DatosRFC.RazonSocial + "\\RECIBIDOS\\" + Mes + "\\";
                        DirDescarga = DescargaRaiz + DirWork;

                        if (!Directory.Exists(DescargaRaiz + DirWork))
                            Directory.CreateDirectory(DescargaRaiz + DirWork);

                        bool consultaFacturasRecibidasResult = false;


                        if (TipoAcceso == "FIEL")
                        {
                            while (!consultaFacturasRecibidasResult)
                                consultaFacturasRecibidasResult = await ConsultarFacturasRecibidasFIEL(RFC_FIEL_CERTIFICATE, RFC_FIEL_PRIVATE_KEY, RFC_FIEL_PASSWORD_PRIVATE_KEY, false);
                        }

                    }
                    break;
            }        
            
            Console.WriteLine("\nProceso terminado, presione una tecla para continuar...");
            //Console.ReadLine();
        }


        static async Task<bool> ConsultarFacturasEmitidasFIEL(string certificateBase64, string privateKeyBase64, string passwordPrivateKey, bool useThreeDays)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

            //Por defecto el limite de conexiones es solo 2 asi que lo aumentamos a 8.
            ServicePointManager.DefaultConnectionLimit = 16;

            byte[] generatedPfx = SecurityHelpers.CerKey2Pfx(certificateBase64, privateKeyBase64, passwordPrivateKey);
            X509Certificate2 cert = new X509Certificate2(generatedPfx, passwordPrivateKey);
            IFlurlClient client = new FlurlClient().WithAutomaticDecompression().WithTimeout(120).EnableCookies();
            client.HttpClient.DefaultRequestHeaders.ExpectContinue = false;

            ISatConnection connection = new FIELConnection(cert, ref client);

            Ref<FacturasEmitidasManager> emitidasManager = new Ref<FacturasEmitidasManager>
            {
                Value = new FacturasEmitidasManager(ref client, ref connection)
            };

            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync(MAX_LOGIN_RETRIES, retryAttempt => TimeSpan.FromSeconds(RETRY_WAIT_TIME), (exception, timeSpan, retryCount, context) =>
                        {
                            // Add logic to be executed before each retry, such as logging
                            Console.WriteLine("Se ha producido la siguiente excepcion: {0}, reintento {1} de {2} ", exception.Message, retryCount, MAX_LOGIN_RETRIES);
                            client.Dispose();
                            client = new FlurlClient().WithAutomaticDecompression().WithTimeout(120).EnableCookies();
                            connection = new FIELConnection(cert, ref client);
                            emitidasManager = new Ref<FacturasEmitidasManager>
                            {
                                Value = new FacturasEmitidasManager(ref client, ref connection)
                            };
                        });
            try
            {
                bool ResultEmitidasLoginAsync = await policy.ExecuteAsync(async () => await emitidasManager.Value.LoginAsync()); // async execute overload
                bool ResultProcesoFacturasEmitidas = await policy.ExecuteAsync(async () => await ProcesoFacturasEmitidas(emitidasManager, useThreeDays)); // async execute overload
                if (!ResultEmitidasLoginAsync || !ResultProcesoFacturasEmitidas)
                    return false;
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Se han superado el total de reintentos de inicio de sesión.");
                return false;
            }
        }
        static async Task<bool> ConsultarFacturasRecibidasFIEL(string certificateBase64, string privateKeyBase64, string passwordPrivateKey, bool useThreeDays)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

            //Configuramos el Logger para utilizar la base de datos


            //Por defecto el limite de conexiones es solo 2 asi que lo aumentamos a 8.
            ServicePointManager.DefaultConnectionLimit = 16;

            byte[] generatedPfx = SecurityHelpers.CerKey2Pfx(certificateBase64, privateKeyBase64, passwordPrivateKey);
            X509Certificate2 cert = new X509Certificate2(generatedPfx, passwordPrivateKey);
            IFlurlClient client = new FlurlClient().WithAutomaticDecompression().WithTimeout(120).EnableCookies();
            client.HttpClient.DefaultRequestHeaders.ExpectContinue = false;

            ISatConnection connection = new FIELConnection(cert, ref client);

            Ref<FacturasRecibidasManager> recibidasManager = new Ref<FacturasRecibidasManager>
            {
                Value = new FacturasRecibidasManager(ref client, ref connection)
            };

            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync(MAX_LOGIN_RETRIES, retryAttempt => TimeSpan.FromSeconds(RETRY_WAIT_TIME), (exception, timeSpan, retryCount, context) =>
                        {
                            // Add logic to be executed before each retry, such as logging
                            Console.WriteLine("Se ha producido la siguiente excepcion: {0}, reintento {1} de {2} ", exception.Message, retryCount, MAX_LOGIN_RETRIES);
                            client.Dispose();
                            client = new FlurlClient().WithAutomaticDecompression().WithTimeout(120).EnableCookies();
                            connection = new FIELConnection(cert, ref client);
                            recibidasManager = new Ref<FacturasRecibidasManager>
                            {
                                Value = new FacturasRecibidasManager(ref client, ref connection)
                            };

                        });

            try
            {
                bool ResultRecibidasLoginAsync = await policy.ExecuteAsync(async () => await recibidasManager.Value.LoginAsync()); // async execute overload
                bool ResultProcesoFacturasRecibidas = await policy.ExecuteAsync(async () => await ProcesoFacturasRecibidas(recibidasManager, useThreeDays)); // async execute overload
                if (!ResultRecibidasLoginAsync || !ResultProcesoFacturasRecibidas)
                    return false;
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Se han superado el total de reintentos de inicio de sesión.");
                return false;
            }
        }


        static async Task<bool> ProcesoFacturasEmitidas(Ref<FacturasEmitidasManager> emitidasManager, bool useTreeDaysHistory)
        {
            if (emitidasManager.Value.IsLoggedIn())
            {
                Console.Write("Iniciando sesión en el portal del SAT..........\n");

                DateTime primerFecha = CONSULTA_PRIMERA_FECHA;
                DateTime segundaFecha = CONSULTA_SEGUNDA_FECHA;

                Ref<SynchronizedCollection<FacturaEmitidaMetadata>> listaFacturasObtenidas = new Ref<SynchronizedCollection<FacturaEmitidaMetadata>>
                {
                    Value = new SynchronizedCollection<FacturaEmitidaMetadata>()
                };

                Console.Write(String.Format("\nObteniendo cantidad de facturas de {0} a {1}..........", primerFecha.ToString("dd/MM/yyyy HH:mm:ss"), segundaFecha.ToString("dd/MM/yyyy HH:mm:ss")));

                var diasEnRango = DateTimeHelper.GetConsultas(primerFecha, segundaFecha);
                await diasEnRango.ForEachAsyncSemaphore(4, x => emitidasManager.Value.ObtenerNumeroFacturasDia(new Tuple<DateTime, DateTime>(x.StartDateTime, x.EndDateTime), listaFacturasObtenidas));

                Console.Write(listaFacturasObtenidas.Value.Count + " Facturas\n");

                var filePaths = Directory.GetFiles(DirDescarga, "*.xml", SearchOption.TopDirectoryOnly).ToList().Select(f => Path.GetFileNameWithoutExtension(f).ToLower()).ToList();
                var listaFacturasObtenidasFoliosFiscales = listaFacturasObtenidas.Value.ToList().Select(x => x.FolioFiscal.ToString().ToLower()).ToList();
                var ListaCfdisADescargar = listaFacturasObtenidasFoliosFiscales.Except(filePaths).ToList();
                var ListaArchivosXMLDescargados = new SynchronizedCollection<string>();
                var ListaDescargasRequeridas = listaFacturasObtenidas.Value.Where(x => ListaCfdisADescargar.Contains(x.FolioFiscal.ToString().ToLower())).ToList();

                //var ListaDescargasRequeridas = listaFacturasObtenidas.Value;
                //SynchronizedCollection<string> ListaArchivosXMLDescargados = new SynchronizedCollection<string>();

                await ListaDescargasRequeridas.ForEachAsyncSemaphore(8, async x => ListaArchivosXMLDescargados.Add(await emitidasManager.Value.DescargarArchivoXML(x, DirDescarga)));
                Console.WriteLine("\nCerrando sesión en el portal SAT..........");
                await emitidasManager.Value.CerrarSesionAsync();
                Console.WriteLine("\nOK");
                return true;
            }
            else
            {
                Console.WriteLine("El login no fue exitoso.");
                await emitidasManager.Value.CerrarSesionAsync();
                return false;
            }
        }
        static async Task<bool> ProcesoFacturasRecibidas(Ref<FacturasRecibidasManager> recibidasManager, bool useTreeDaysHistory)
        {
            if (recibidasManager.Value.IsLoggedIn())
            {
                Console.WriteLine("Iniciando sesión en el portal del SAT..........");

                DateTime primerFecha = CONSULTA_PRIMERA_FECHA;
                DateTime segundaFecha = CONSULTA_SEGUNDA_FECHA;

                Ref<SynchronizedCollection<FacturaRecibidaMetadata>> listaFacturasObtenidas = new Ref<SynchronizedCollection<FacturaRecibidaMetadata>>
                {
                    Value = new SynchronizedCollection<FacturaRecibidaMetadata>()
                };

                Console.WriteLine("Obteniendo cantidad de facturas de {0} a {1}:", primerFecha.ToString("dd/MM/yyyy HH:mm:ss"), segundaFecha.ToString("dd/MM/yyyy HH:mm:ss"));

                var diasEnRango = DateTimeHelper.GetConsultas(primerFecha, segundaFecha);
                await diasEnRango.ForEachAsyncSemaphore(4, x => recibidasManager.Value.ObtenerNumeroFacturasDia(new Tuple<DateTime, DateTime>(x.StartDateTime, x.EndDateTime), listaFacturasObtenidas));

                Console.Write(listaFacturasObtenidas.Value.Count + " Facturas \n");

                var filePaths = Directory.GetFiles(DirDescarga, "*.xml", SearchOption.TopDirectoryOnly).ToList().Select(f => Path.GetFileNameWithoutExtension(f).ToLower()).ToList();
                var listaFacturasObtenidasFoliosFiscales = listaFacturasObtenidas.Value.ToList().Select(x => x.FolioFiscal.ToString().ToLower()).ToList();
                var ListaCfdisADescargar = listaFacturasObtenidasFoliosFiscales.Except(filePaths).ToList();
                var ListaArchivosXMLDescargados = new SynchronizedCollection<string>();
                var ListaDescargasRequeridas = listaFacturasObtenidas.Value.Where(x => ListaCfdisADescargar.Contains(x.FolioFiscal.ToString().ToLower())).ToList();


                //SynchronizedCollection<string> ListaArchivosXMLDescargados = new SynchronizedCollection<string>();
                //var ListaDescargasRequeridas = listaFacturasObtenidas.Value;

                await ListaDescargasRequeridas.ForEachAsyncSemaphore(8, async x => ListaArchivosXMLDescargados.Add(await recibidasManager.Value.DescargarArchivoXML(x, DirDescarga)));

                Console.WriteLine("\nCerrando sesión en el portal SAT:");
                await recibidasManager.Value.CerrarSesionAsync();
                Console.WriteLine("OK");
                return true;
            }
            else
            {
                Console.WriteLine("El login no fue exitoso.");
                await recibidasManager.Value.CerrarSesionAsync();
                return false;
            }
        }
    }
}
