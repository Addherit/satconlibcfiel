﻿using System;

namespace SATCONLIB
{
    public class DateTimeRange
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}