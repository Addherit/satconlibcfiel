﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class RFCData
    {
        public int Id { get; set; }
        public string Rfc { get; set; }
        public string RazonSocial { get; set; }
        public int EstadoRfc { get; set; }
        public byte[] Certificado { get; set; }
        public byte[] ClavePrivada { get; set; }
        public string PasswordClavePrivada { get; set; }
        public string PasswordClaveCIEC { get; set; }
        public string DirCapcha { get; set; }
    }
}
