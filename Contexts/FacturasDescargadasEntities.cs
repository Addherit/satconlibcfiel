﻿namespace SATCONLIB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class FacturasDescargadasEntities : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'FacturasDescargadasEntities' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'SATCONLIB.FacturasDescargadasEntities' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'FacturasDescargadasEntities'  en el archivo de configuración de la aplicación.

        private string Schema;
        public FacturasDescargadasEntities(string connectionString, string schema)
        {
            this.Database.Connection.ConnectionString = connectionString;
            this.Schema = schema;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // configures one-to-many relationship
            modelBuilder.Entity<RFC>()
                .HasRequired<Cliente>(b => b.cliente)
                .WithMany(a => a.Rfcs)
                .HasForeignKey<int>(b => b.clienteId);

                modelBuilder.Entity<Aerolineas>().ToTable("Aerolineas", this.Schema);
                modelBuilder.Entity<AerolineasOtrosCargos>().ToTable("AerolineasOtrosCargos", this.Schema);
                modelBuilder.Entity<AerolineasOtrosCargosCargo>().ToTable("AerolineasOtrosCargosCargo", this.Schema);
                modelBuilder.Entity<Comprobante>().ToTable("Comprobante", this.Schema);
                modelBuilder.Entity<ComprobanteAddenda>().ToTable("ComprobanteAddenda", this.Schema);
                modelBuilder.Entity<ComprobanteAddendaCustomized>().ToTable("ComprobanteAddendaCustomized", this.Schema);
                modelBuilder.Entity<ComprobanteAddendaCustomizedAdditionalInformation>().ToTable("ComprobanteAddendaCustomizedAdditionalInformation", this.Schema);
                modelBuilder.Entity<ComprobanteAddendaCustomizedDatosReceptorDireccion>().ToTable("ComprobanteAddendaCustomizedDatosReceptorDireccion", this.Schema);
                modelBuilder.Entity<ComprobanteCfdiRelacionados>().ToTable("ComprobanteCfdiRelacionados", this.Schema);
                modelBuilder.Entity<ComprobanteCfdiRelacionadosCfdiRelacionado>().ToTable("ComprobanteCfdiRelacionadosCfdiRelacionado", this.Schema);
                modelBuilder.Entity<ComprobanteComplemento>().ToTable("ComprobanteComplemento", this.Schema);
                modelBuilder.Entity<ComprobanteConcepto>().ToTable("ComprobanteConcepto", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoComplementoConcepto>().ToTable("ComprobanteConceptoComplementoConcepto", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoCuentaPredial>().ToTable("ComprobanteConceptoCuentaPredial", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoImpuestos>().ToTable("ComprobanteConceptoImpuestos", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoImpuestosRetencion>().ToTable("ComprobanteConceptoImpuestosRetencion", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoImpuestosTraslado>().ToTable("ComprobanteConceptoImpuestosTraslado", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoInformacionAduanera>().ToTable("ComprobanteConceptoInformacionAduanera", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoParte>().ToTable("ComprobanteConceptoParte", this.Schema);
                modelBuilder.Entity<ComprobanteConceptoParteInformacionAduanera>().ToTable("ComprobanteConceptoParteInformacionAduanera", this.Schema);
                modelBuilder.Entity<ComprobanteEmisor>().ToTable("ComprobanteEmisor", this.Schema);
                modelBuilder.Entity<ComprobanteImpuestos>().ToTable("ComprobanteImpuestos", this.Schema);
                modelBuilder.Entity<ComprobanteImpuestosRetencion>().ToTable("ComprobanteImpuestosRetencion", this.Schema);
                modelBuilder.Entity<ComprobanteImpuestosTraslado>().ToTable("ComprobanteImpuestosTraslado", this.Schema);
                modelBuilder.Entity<ComprobanteReceptor>().ToTable("ComprobanteReceptor", this.Schema);
                modelBuilder.Entity<FacturaEmitidaMetadata>().ToTable("FacturaEmitidaMetadata", this.Schema);
                modelBuilder.Entity<FacturaRecibidaMetadata>().ToTable("FacturaRecibidaMetadata", this.Schema);
                modelBuilder.Entity<Nomina>().ToTable("Nomina", this.Schema);
                modelBuilder.Entity<NominaDeducciones>().ToTable("NominaDeducciones", this.Schema);
                modelBuilder.Entity<NominaDeduccionesDeduccion>().ToTable("NominaDeduccionesDeduccion", this.Schema);
                modelBuilder.Entity<NominaEmisor>().ToTable("NominaEmisor", this.Schema);
                modelBuilder.Entity<NominaEmisorEntidadSNCF>().ToTable("NominaEmisorEntidadSNCF", this.Schema);
                modelBuilder.Entity<NominaIncapacidad>().ToTable("NominaIncapacidad", this.Schema);
                modelBuilder.Entity<NominaOtroPago>().ToTable("NominaOtroPago", this.Schema);
                modelBuilder.Entity<NominaOtroPagoCompensacionSaldosAFavor>().ToTable("NominaOtroPagoCompensacionSaldosAFavor", this.Schema);
                modelBuilder.Entity<NominaOtroPagoSubsidioAlEmpleo>().ToTable("NominaOtroPagoSubsidioAlEmpleo", this.Schema);
                modelBuilder.Entity<NominaPercepciones>().ToTable("NominaPercepciones", this.Schema);
                modelBuilder.Entity<NominaPercepcionesJubilacionPensionRetiro>().ToTable("NominaPercepcionesJubilacionPensionRetiro", this.Schema);
                modelBuilder.Entity<NominaPercepcionesPercepcion>().ToTable("NominaPercepcionesPercepcion", this.Schema);
                modelBuilder.Entity<NominaPercepcionesPercepcionAccionesOTitulos>().ToTable("NominaPercepcionesPercepcionAccionesOTitulos", this.Schema);
                modelBuilder.Entity<NominaPercepcionesPercepcionHorasExtra>().ToTable("NominaPercepcionesPercepcionHorasExtra", this.Schema);
                modelBuilder.Entity<NominaPercepcionesSeparacionIndemnizacion>().ToTable("NominaPercepcionesSeparacionIndemnizacion", this.Schema);
                modelBuilder.Entity<NominaReceptor>().ToTable("NominaReceptor", this.Schema);
                modelBuilder.Entity<NominaReceptorSubContratacion>().ToTable("NominaReceptorSubContratacion", this.Schema);
                modelBuilder.Entity<Pagos>().ToTable("Pagos", this.Schema);
                modelBuilder.Entity<PagosPago>().ToTable("PagosPago", this.Schema);
                modelBuilder.Entity<PagosPagoDoctoRelacionado>().ToTable("PagosPagoDoctoRelacionado", this.Schema);
                modelBuilder.Entity<PagosPagoImpuestos>().ToTable("PagosPagoImpuestos", this.Schema);
                modelBuilder.Entity<PagosPagoImpuestosRetencion>().ToTable("PagosPagoImpuestosRetencion", this.Schema);
                modelBuilder.Entity<PagosPagoImpuestosTraslado>().ToTable("PagosPagoImpuestosTraslado", this.Schema);
                modelBuilder.Entity<TimbreFiscalDigital>().ToTable("TimbreFiscalDigital", this.Schema);
            }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<FacturaEmitidaMetadata> FacturasEmitidas { get; set; }
        public virtual DbSet<FacturaRecibidaMetadata> FacturasRecibidas { get; set; }
        public virtual DbSet<Comprobante> Comprobantes { get; set; }
        public virtual DbSet<RFC> RFCs { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}