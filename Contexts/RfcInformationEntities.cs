﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SATCONLIB
{
    public class RfcInformationEntities: DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'FacturasDescargadasEntities' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'SATCONLIB.FacturasDescargadasEntities' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'FacturasDescargadasEntities'  en el archivo de configuración de la aplicación.

        private string Schema;
        public RfcInformationEntities(string connectionString, string schema)
        {
            this.Database.Connection.ConnectionString = connectionString;
            this.Schema = schema;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            // configures one-to-many relationship
            modelBuilder.Entity<RFC>()
                .HasRequired<Cliente>(b => b.cliente)
                .WithMany(a => a.Rfcs)
                .HasForeignKey<int>(b => b.clienteId);
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<RFC> RFCs { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
    }
}
